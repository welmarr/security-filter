package tuto.spring.securityfilter.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tuto.spring.securityfilter.models.User;

//https://www.oracle.com/java/technologies/data-access-object.html
//https://en.wikipedia.org/wiki/Data_access_object
//https://stackoverflow.com/questions/19154202/data-access-object-dao-in-java
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  Optional<User> findByEmail(String email);
  Boolean existsByEmail(String email);
}