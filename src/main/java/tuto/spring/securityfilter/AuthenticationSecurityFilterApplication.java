package tuto.spring.securityfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthenticationSecurityFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationSecurityFilterApplication.class, args);
	}

}
