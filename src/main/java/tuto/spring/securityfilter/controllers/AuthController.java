package tuto.spring.securityfilter.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import tuto.spring.securityfilter.errors.ResourceExistedException;
import tuto.spring.securityfilter.errors.ResourceNotFoundException;
import tuto.spring.securityfilter.models.User;
import tuto.spring.securityfilter.repositories.UserRepository;
import tuto.spring.securityfilter.services.AuthService;
import tuto.spring.securityfilter.services.security.UserDetailsImplementation;
import tuto.spring.securityfilter.utils.JwtUtils;
import tuto.spring.securityfilter.utils.enums.ErrorMsg;
import tuto.spring.securityfilter.utils.enums.ResponseStatus;
import tuto.spring.securityfilter.utils.formats.requests.UserLoginRequest;
import tuto.spring.securityfilter.utils.formats.requests.UserRegistrationRequest;
import tuto.spring.securityfilter.utils.formats.responses.Response;
import tuto.spring.securityfilter.utils.formats.responses.UserLoginResponse;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthService authService;
    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserRepository userRepository;

    @PostMapping("/login")
    public ResponseEntity<Response> login(@Valid @RequestBody UserLoginRequest loginRequest) throws ResourceNotFoundException {

        if (!userRepository.existsByEmail(loginRequest.getEmail()).booleanValue()) {
            throw new ResourceNotFoundException(ErrorMsg.RESOURCE_NOT_FOUND_ENTITY.getValue()
                    .replace("%entity%", "User").replace("%id%", loginRequest.getEmail()), null);
        }

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),
                        loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImplementation userDetails = (UserDetailsImplementation) authentication.getPrincipal();
        ResponseCookie cookie = jwtUtils.generateJwtCookie(userDetails);

        Response out = new Response();
        out.setStatus(ResponseStatus.SUCCESS.getValue());
        out.setData(new UserLoginResponse(userDetails.getId(), userDetails.getEmail()));
        
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
                .body(out);

    }

    @PostMapping("/register")
    public ResponseEntity<Response> register(@Valid @RequestBody UserRegistrationRequest userRegistrationRequest) throws ResourceExistedException {

        ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
        Response out = authService.register(userRegistrationRequest);

        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
                .body(out);
    }
}
