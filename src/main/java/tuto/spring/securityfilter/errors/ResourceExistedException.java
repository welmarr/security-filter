package tuto.spring.securityfilter.errors;

import java.util.HashMap;


public class ResourceExistedException extends Exception{
    private HashMap<String, Object> info;
    public ResourceExistedException(String message, HashMap<String, Object> info) {
        super(message);
        this.info = info;
    }

}
