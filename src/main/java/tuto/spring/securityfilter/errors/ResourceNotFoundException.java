package tuto.spring.securityfilter.errors;

import java.util.HashMap;


public class ResourceNotFoundException extends Exception{
    private HashMap<String, Object> info;
    public ResourceNotFoundException(String message, HashMap<String, Object> info) {
        super(message);
        this.info = info;
    }

}
