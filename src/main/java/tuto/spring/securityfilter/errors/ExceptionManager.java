package tuto.spring.securityfilter.errors;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import tuto.spring.securityfilter.utils.enums.ResponseStatus;
import tuto.spring.securityfilter.utils.formats.responses.Response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ControllerAdvice
public class ExceptionManager extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Response> resourceNotFoundException(ResourceNotFoundException ex, WebRequest wb){
        Response out = new Response();
        out.setMsg(ex.getMessage());
        out.setCode(HttpStatus.NOT_FOUND.value());
        out.setStatus(ResponseStatus.FAILED.getValue());        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(out);
    }

    

    @ExceptionHandler(ResourceExistedException.class)
    public ResponseEntity<Response> resourceExistedException(ResourceExistedException ex, WebRequest wb){
        Response out = new Response();
        out.setMsg(ex.getMessage());
        out.setCode(HttpStatus.CONFLICT.value());
        out.setStatus(ResponseStatus.FAILED.getValue());        
        return ResponseEntity.status(HttpStatus.CONFLICT).body(out);
    }

    

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Response> exception(Exception ex, WebRequest wb){
        Response out = new Response();
        out.setMsg(ex.getMessage());
        out.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        out.setStatus(ResponseStatus.FAILED.getValue());        
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(out);
    }
}
