package tuto.spring.securityfilter.utils.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ResponseStatus {
    SUCCESS("Success."),
    FAILED("Failed.");

    private String value;
}
