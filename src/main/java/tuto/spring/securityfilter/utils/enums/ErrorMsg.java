package tuto.spring.securityfilter.utils.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorMsg {
    RESOURCE_EXISTED("The resource [%entity%:%id%] already existed."),
    RESOURCE_NOT_FOUND_ENTITY("The resource [%entity%:%id%] not found.");
    private String value;
    
    
}
