package tuto.spring.securityfilter.utils.formats.requests;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserRegistrationRequest {

    @NotBlank
    @Size(min = 5, max = 50)
    @Email
    String email;

    @NotBlank
    @Size(min = 5, max = 50)
    String password;
}
