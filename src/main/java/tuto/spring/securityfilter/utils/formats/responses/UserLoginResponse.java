package tuto.spring.securityfilter.utils.formats.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserLoginResponse {
    Long id;
    String email;
}
