package tuto.spring.securityfilter.utils.formats.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tuto.spring.securityfilter.utils.enums.ResponseStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    private final static Response i = new Response();
    private String status;
    private String msg;
    private Integer code;
    private Object data;

    public static Response success(Integer code){
        i.setCode(code);
        i.setStatus(ResponseStatus.SUCCESS.getValue());
        return i;
    }
}
