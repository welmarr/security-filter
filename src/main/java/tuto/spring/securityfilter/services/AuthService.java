package tuto.spring.securityfilter.services;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import jakarta.validation.Valid;
import tuto.spring.securityfilter.errors.ResourceExistedException;
import tuto.spring.securityfilter.models.User;
import tuto.spring.securityfilter.repositories.UserRepository;
import tuto.spring.securityfilter.utils.JwtUtils;
import tuto.spring.securityfilter.utils.enums.ErrorMsg;
import tuto.spring.securityfilter.utils.formats.requests.UserRegistrationRequest;
import tuto.spring.securityfilter.utils.formats.responses.Response;

@Service
public class AuthService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    AuthenticationManager authenticationManager;

    public Response register(@Valid @RequestBody UserRegistrationRequest urr) throws ResourceExistedException {

        Response rps = new Response();

        rps.setCode(200);

        if (userRepository.existsByEmail(urr.getEmail()).booleanValue()) {
            throw new ResourceExistedException(ErrorMsg.RESOURCE_EXISTED.getValue()
                    .replace("%entity%", "User").replace("%id%", urr.getEmail()), null);
        }

        User user = new User(urr.getEmail(), encoder.encode(urr.getPassword()));
        userRepository.save(user);
        rps.setData(user);
        rps.setCode(200);

        return rps;

    }

}
