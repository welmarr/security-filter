package tuto.spring.securityfilter.services.security;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import tuto.spring.securityfilter.models.User;

//https://spring.io/guides/gs/securing-web/
//https://docs.spring.io/spring-security/reference/servlet/authentication/index.html
@Data
public class UserDetailsImplementation implements UserDetails {

    private static final long serialVersionUID = 1L;

    private Long id;
  
    private String email;

    @JsonIgnore
    private String password;


    public UserDetailsImplementation(Long id, String email, String password){
        this.id = id;
        this.email = email;
        this.password = password;
    }


    public UserDetailsImplementation(User user){
        this(user.getId(), user.getEmail(), user.getPassword());
    }
  
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    
}
